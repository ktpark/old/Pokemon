﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBotOrganize
{
    class Program
    {
        static string baseDir;
        static void Main(string[] args)
        {
            baseDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Pokemon Go");
            if(!Directory.Exists(baseDir))
            {
                Console.WriteLine("Folder not found: " + baseDir);
                return;
            }

            string accountsDir = Path.Combine(baseDir, "Accounts");
            if (Directory.Exists(accountsDir))
                Directory.Delete(accountsDir, true);
            Directory.CreateDirectory(accountsDir);

            string[][] accounts = readAccounts();
            for(int i=0; i<accounts.Length; i++)
            {
                Organize account = new Organize(accounts[i]);
                account.binary();
                account.config();
            }

            Console.WriteLine("Setup has completed. Press any key to continue...");
            Console.Read();
        }

        static string[][] readAccounts()
        {
            string path = Path.Combine(baseDir, "Accounts.txt");
            string[] lines = System.IO.File.ReadAllLines(path);
            string[][] logins = new string[lines.Length][];
            for(int i=0; i<logins.Length; i++)
                logins[i] = lines[i].Split(null);
            return logins;  
        }

    }
}
