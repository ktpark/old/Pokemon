﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBotOrganize
{
    class Organize
    {

        private string baseDir;
        private string userDir;
        private string[] account;

        public Organize(string[] account)
        {
            baseDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"Pokemon Go");
            userDir = Path.Combine(baseDir, @"Accounts\" + account[0] + "-" + account[1]);
            this.account = account;
            if (!Directory.Exists(userDir))
                Directory.CreateDirectory(userDir);
        }

        public void binary()
        {
            string binaryPath = Path.Combine(baseDir, @"NecroBot\PoGo.NecroBot.CLI\bin\x86\Release");
            RecursiveCopy(binaryPath, userDir);
        }

        public void config()
        {
            string folder = Path.Combine(userDir, "Config");
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            string file = "config.json";
            File.Copy(Path.Combine(baseDir, file), Path.Combine(folder, file), true);
            file = "auth.json";
            File.WriteAllText(Path.Combine(folder, file), auth());
        }

        private string auth()
        {
            string text = "{\n";
            text += "  \"AuthConfig\": {\n";
            text += "    \"AuthType\": \"" + account[0] + "\",\n";
            if(account[0].Equals("google"))
            {
                text += "    \"GoogleUsername\": \"" + account[1] + "\",\n";
                text += "    \"GooglePassword\": \"" + account[2] + "\"\n";
            }
            else
            {
                text += "    \"PtcUsername\": \"" + account[1] + "\",\n";
                text += "    \"PtcPassword\": \"" + account[2] + "\"\n";
            }
            text += "  },\n";

            text += "  \"ProxyConfig\": {\n";
            text += "    \"UseProxy\": false,\n";
            text += "    \"UseProxyHost\": null,\n";
            text += "    \"UseProxyPort\": null,\n";
            text += "    \"UseProxyAuthentication\": false,\n";
            text += "    \"UseProxyUsername\": null,\n";
            text += "    \"UseProxyPassword\": null\n";
            text += "  },\n";
            
            text += "  \"DeviceConfig\": {\n";
            text += "    \"DevicePackageName\": \"lg-v10\",\n";
            text += "    \"DeviceId\": \"" + account[3] + "\",\n";
            text += "    \"AndroidBoardName\": \"MSM8992\",\n";
            text += "    \"AndroidBootloader\": \"\",\n";
            text += "    \"DeviceBrand\": \"LG\",\n";
            text += "    \"DeviceModel\": \"V10\",\n";
            text += "    \"DeviceModelIdentifier\": \"pplus\",\n";
            text += "    \"DeviceModelBoot\": \"qcom\",\n";
            text += "    \"HardwareManufacturer\": \"LG\",\n";
            text += "    \"HardwareModel\": \"V10\",\n";
            text += "    \"FirmwareBrand\": \"pplus\",\n";
            text += "    \"FirmwareTags\": \"test-keys\",\n";
            text += "    \"FirmwareType\": \"eng\",\n";
            text += "    \"FirmwareFingerprint\": \"LG/pplus/pplus:5.1.1/LYZ28J/kasp3rd02071120:eng/test-keys\"\n";
            text += "  }\n";
            text += "}";
            return text;
        }

        private void RecursiveCopy(string sourceDir, string destDir)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDir);

            DirectoryInfo[] dirs = dir.GetDirectories();
            if (!Directory.Exists(destDir))
                Directory.CreateDirectory(destDir);

            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string tempPath = Path.Combine(destDir, file.Name);
                file.CopyTo(tempPath, true);
            }
            
            foreach (DirectoryInfo subdir in dirs)
            {
                string tempPath = Path.Combine(destDir, subdir.Name);
                RecursiveCopy(subdir.FullName, tempPath);
            }
        }

    }
}
