import mechanize, cookielib
import sys, time, geopt

url = "https://club.pokemon.com/us/pokemon-trainer-club/sign-up/"	

minimumAccount = 37
maximumAccount = 60

while minimumAccount <= maximumAccount:

    br = mechanize.Browser()
    cj = cookielib.LWPCookieJar()
    br.set_cookiejar(cj)
    br.set_handle_equiv(True)
    br.set_handle_gzip(True)
    br.set_handle_redirect(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)
    #br.set_handle_redirect(True)
    br.addheaders = [('User-agent','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36'),
                     ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),
                     ('Accept-Encoding', 'gzip, deflate, sdch, br'),
                     ('Accept-Language', 'en-US,en;q=0.8,ko;q=0.6,es;q=0.4')]
    
    #Age Verification
    br.open(url)
    br.select_form("verify-age")
    br.form["dob"] = "1995-07-04"
    #br.form["country"] = "US"
    br.submit()
    
    #Parents Sign Up
    br.select_form("create-account")
    br.form["username"] = "test123+{0}".format(minimumAccount)
    br.form["password"] = "123abc"
    br.form["confirm_password"] = "123abc"
    br.form["email"] = "testabc+{0}@gmail.com".format(minimumAccount)
    br.form["confirm_email"] = "testabc+{0}@gmail.com".format(minimumAccount)
    br.find_control("email_opt_in").items[0].selected=False
    br.form["screen_name"] = "test123{0}".format(minimumAccount)
    br.find_control("terms").items[0].selected=True
    result = br.submit()
    
    print("Account {0}: {1}".format(minimumAccount, result.geturl()))
    
    minimumAccount += 1
    time.sleep(10)
	
print("Account Generation Complete")
