import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;

public class Driver {
	
	final static int minDuplicate = 1;
	final static float minIV = 95.f;
	final static float percentageCP = .8f;

	public static void main(String[] args) throws IOException {
		String output = "  \"PokemonsTransferFilter\": {\n";
		Parse cp = new Parse("MaxPokemonCP.txt");
		Parse moves = new Parse("BestPokemonMoves.txt");
		String[] pokemonCP, pokemonMoves;
		
		Hashtable<String, String[]> movesTable = new Hashtable<String, String[]>();
		while((pokemonMoves = moves.readLine()) != null)
			movesTable.put(pokemonMoves[0], pokemonMoves);
		
		while((pokemonCP = cp.readLine()) != null) {
			String bestMoves = "";
			pokemonMoves = movesTable.get(pokemonCP[1]);
			/*if(pokemonMoves != null)
			{
				for(int i=1; i<pokemonMoves.length; i++)
					bestMoves += "\"" + pokemonMoves[i] + "\", ";
				bestMoves = bestMoves.substring(0, bestMoves.length() - 2);
			}*/
			
			output += "    \"" + pokemonCP[1] + "\": {\n";
			output += "      \"KeepMinCp\": " + (int)(Float.parseFloat(pokemonCP[2]) * percentageCP) + ",\n";
			output += "      \"KeepMinLvl\" : 6,\n";
			output += "      \"UseKeepMinLvl\" : false,\n";
			output += "      \"KeepMinIvPercentage\": " + String.format("%.1f", minIV) + ",\n";
			output += "      \"KeepMinDuplicatePokemon\": " + minDuplicate + ",\n";
			output += "      \"Moves\": [" + bestMoves + "],\n";
			output += "      \"DeprecatedMoves\" : null,\n";
			output += "      \"KeepMinOperator\" : \"or\",\n";
			output += "      \"MovesOperator\" : \"or\"\n";
			output += "    },\n";
		}
		cp.close();
		
		output = output.substring(0, output.length() - 2) + "\n";
		output += "  },\n";
		
		PrintWriter writer = new PrintWriter("PokemonTransferFilter.txt", "UTF-8");
		writer.print(output);
		writer.close();

	}

}
