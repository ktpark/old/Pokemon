import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class Parse {
	
	BufferedReader br;
	
	public Parse(String file) throws FileNotFoundException {
		FileInputStream fstream = new FileInputStream(file);
		br = new BufferedReader(new InputStreamReader(fstream));
	}
	
	public String[] readLine() throws IOException {
		String line = br.readLine();
		if(line == null)
			return null;
		return line.split("\t");
	}
	
	public void close() throws IOException {
		br.close();
	}
	
}
